﻿import typer

from config_admin.misc.enums import CloudProvider
from config_admin.services.config_service import ConfigService
from config_admin.services.requirement_service import RequirementService

config_service = ConfigService()
requirement_service = RequirementService()

app = typer.Typer()
app_snapshot = typer.Typer()
app.add_typer(app_snapshot, name="snapshot")

app_requirement = typer.Typer()
app.add_typer(app_requirement, name="requirement")

@app_snapshot.command("create")
def create_snapshot(cloud: CloudProvider = typer.Option(...),
                            subscription: str = typer.Option(...),
                            resource_group_list: str = typer.Option(...)):
    config_service.create_snapshot(cloud, subscription, resource_group_list)

@app_snapshot.command("list")
def list_snapshot(cloud: CloudProvider = typer.Option(...),
                  commit: str = typer.Option(...)):
    config_service.list_snapshot(cloud, commit)

@app_snapshot.command("list-all")
def list_snapshots(cloud: CloudProvider = typer.Option(...)):
    config_service.list_snapshots(cloud)

@app_snapshot.command("summary")
def summary_snapshot(cloud: CloudProvider = typer.Option(...),
                             commit: str = typer.Option(...)):
    config_service.summary_snapshot(cloud, commit)
    pass

@app_requirement.command("check")
def check_requirement(config: str = typer.Option(...)):
    requirement_service.check_requirement(config)

if __name__ == "__main__":
    app()

