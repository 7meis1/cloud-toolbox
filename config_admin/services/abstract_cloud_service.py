from abc import ABC, abstractclassmethod

class AbstractCloudService(ABC):

    @abstractclassmethod
    def get_config(self):
        pass
