import os
from config_admin.misc.const import DEFAULT_CONFIG_PATH
from config_admin.misc.const import AWS_CONFIG_PATH
from config_admin.misc.helper import *
from config_admin.services.abstract_cloud_service import AbstractCloudService
import boto3


# according to:
#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/networkmanager.html

class AWSService(AbstractCloudService):

    def __init__(self) -> None:
        self.session = boto3.Session(profile_name='default')

    def get_config(self):
        pass

    @property
    def git_repo_path(self):
        return os.path.join(DEFAULT_CONFIG_PATH, AWS_CONFIG_PATH)

    def check_vpc(self, rule: dict):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_type = properties.get('vpc_type', '')
        vpc_list = list(ec2.vpcs.filter(Filters=[]))

        #[{'CidrBlock': '172.31.0.0/16', 'DhcpOptionsId': 'dopt-0b237970158e53561', 'State': 'available', 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664', 'InstanceTenancy': 'default', 'CidrBlockAssociationSet': [{'AssociationId': 'vpc-cidr-assoc-0824db379c92fa721', 'CidrBlock': '172.31.0.0/16', 'CidrBlockState': {'State': 'associated'}}], 'IsDefault': True}]

        for vpc in vpc_list:
            vpc_id = vpc.id
            if vpc_type in vpc_id:
                vpc_data = vpc_client.describe_vpcs(Filters=[])
                vpc_details = vpc_data['Vpcs']
                check_result_vpc = check_expected_properties(expected_properties, vpc_details)
                print(f'Name: {vpc_id} requirement check: {check_result_vpc}')

            else:
                print(f'Name: {vpc_id} has no check!')
        print_summary
        print_resource_summary(resource_name, vpc_list)

    def check_subnet(self, rule: dict):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_list = list(ec2.vpcs.filter(Filters=[]))
        for vpc in vpc_list:
            vpc_id = vpc.id
            subnet_list = vpc_client.describe_subnets(Filters=[{"Name": "vpc-id",
                                                    "Values": [vpc_id]}])['Subnets']

            # [{'AvailabilityZone': 'eu-west-1c', 'AvailabilityZoneId': 'euw1-az3', 'AvailableIpAddressCount': 4091, 'CidrBlock': '172.31.32.0/20', 'DefaultForAz': True, 'MapPublicIpOnLaunch': True, 'MapCustomerOwnedIpOnLaunch': False, 'State': 'available', 'SubnetId': 'subnet-09f29ca8eb6da975d', 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664', 'AssignIpv6AddressOnCreation': False, 'Ipv6CidrBlockAssociationSet': [], 'SubnetArn': 'arn:aws:ec2:eu-west-1:309438011664:subnet/subnet-09f29ca8eb6da975d', 'EnableDns64': False, 'Ipv6Native': False, 'PrivateDnsNameOptionsOnLaunch': {'HostnameType': 'ip-name', 'EnableResourceNameDnsARecord': False, 'EnableResourceNameDnsAAAARecord': False}}, {'AvailabilityZone': 'eu-west-1a', 'AvailabilityZoneId': 'euw1-az1', 'AvailableIpAddressCount': 4091, 'CidrBlock': '172.31.0.0/20', 'DefaultForAz': True, 'MapPublicIpOnLaunch': True, 'MapCustomerOwnedIpOnLaunch': False, 'State': 'available', 'SubnetId': 'subnet-01880e304d2c7795b', 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664', 'AssignIpv6AddressOnCreation': False, 'Ipv6CidrBlockAssociationSet': [], 'SubnetArn': 'arn:aws:ec2:eu-west-1:309438011664:subnet/subnet-01880e304d2c7795b', 'EnableDns64': False, 'Ipv6Native': False, 'PrivateDnsNameOptionsOnLaunch': {'HostnameType': 'ip-name', 'EnableResourceNameDnsARecord': False, 'EnableResourceNameDnsAAAARecord': False}}, {'AvailabilityZone': 'eu-west-1b', 'AvailabilityZoneId': 'euw1-az2', 'AvailableIpAddressCount': 4091, 'CidrBlock': '172.31.16.0/20', 'DefaultForAz': True, 'MapPublicIpOnLaunch': True, 'MapCustomerOwnedIpOnLaunch': False, 'State': 'available', 'SubnetId': 'subnet-00b6d2337ca24f7fc', 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664', 'AssignIpv6AddressOnCreation': False, 'Ipv6CidrBlockAssociationSet': [], 'SubnetArn': 'arn:aws:ec2:eu-west-1:309438011664:subnet/subnet-00b6d2337ca24f7fc', 'EnableDns64': False, 'Ipv6Native': False, 'PrivateDnsNameOptionsOnLaunch': {'HostnameType': 'ip-name', 'EnableResourceNameDnsARecord': False, 'EnableResourceNameDnsAAAARecord': False}}]

            subnet_type = properties.get('subnet_type', '')
            for subnet in subnet_list:
                subnet_id = subnet['SubnetId']
                if subnet_type in subnet_id:
                    check_result_subnet = check_expected_properties(expected_properties, subnet)
                    print(f'Name: {subnet_id} requirement check: {check_result_subnet}')
                else:
                    print(f'Name: {subnet_id} has no check!')

        print_summary
        print_resource_summary(resource_name, subnet_list)


    def check_route_table(self, rule: dict):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_list = list(ec2.vpcs.filter(Filters=[]))
        for vpc in vpc_list:
            vpc_id = vpc.id
        r_table_list = vpc_client.describe_route_tables(Filters=[{"Name": "vpc-id",
                                                      "Values": [vpc_id]}])['RouteTables']

        # [{'Associations': [{'Main': True, 'RouteTableAssociationId': 'rtbassoc-0cdc461a09573ea6d', 'RouteTableId': 'rtb-05913509955800741', 'AssociationState': {'State': 'associated'}}], 'PropagatingVgws': [], 'RouteTableId': 'rtb-05913509955800741', 'Routes': [{'DestinationCidrBlock': '172.31.0.0/16', 'GatewayId': 'local', 'Origin': 'CreateRouteTable', 'State': 'active'}, {'DestinationCidrBlock': '0.0.0.0/0', 'GatewayId': 'igw-098d3e75355126e59', 'Origin': 'CreateRoute', 'State': 'active'}], 'Tags': [], 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664'}]

        route_table_type = properties.get('route_table_type', '')
        for rt in r_table_list:
            rt_id = rt['Associations'][0]['RouteTableId']
            if route_table_type in rt_id:
                check_result_subnet = check_expected_properties(expected_properties, rt)
                print(f'Name: {rt_id} requirement check: {check_result_subnet}')
            else:
                print(f'Name: {rt_id} has no check!')

        print_summary
        print_resource_summary(resource_name, r_table_list)


    def check_security_group(self, rule):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_list = list(ec2.vpcs.filter(Filters=[]))
        for vpc in vpc_list:
            vpc_id = vpc.id
            sgs_list = vpc_client.describe_security_groups(Filters=[{"Name": "vpc-id",
                                                        "Values": [vpc_id]}])['SecurityGroups']

        # [{'Description': 'default VPC security group', 'GroupName': 'default', 'IpPermissions': [{'IpProtocol': '-1', 'IpRanges': [], 'Ipv6Ranges': [], 'PrefixListIds': [], 'UserIdGroupPairs': [{'GroupId': 'sg-0ba0016eae2f00fa4', 'UserId': '309438011664'}]}], 'OwnerId': '309438011664', 'GroupId': 'sg-0ba0016eae2f00fa4', 'IpPermissionsEgress': [{'IpProtocol': '-1', 'IpRanges': [{'CidrIp': '0.0.0.0/0'}], 'Ipv6Ranges': [], 'PrefixListIds': [], 'UserIdGroupPairs': []}], 'VpcId': 'vpc-0a854586a432f8f99'}]

            sgs_type = properties.get('sgs_type', '')
            for sgs in sgs_list:
                sgs_id = sgs['GroupId']
                if sgs_type in sgs_id:
                    check_result_sgs = check_expected_properties(expected_properties, sgs)
                    print(f'Name: {sgs_id} requirement check: {check_result_sgs}')
                else:
                    print(f'Name: {sgs_id} has no check!')

        print_summary
        print_resource_summary(resource_name, sgs_list)

    def check_network_acl(self, rule):
        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_list = list(ec2.vpcs.filter(Filters=[]))
        for vpc in vpc_list:
            vpc_id = vpc.id
            acl_list = vpc_client.describe_network_acls(Filters=[{"Name": "vpc-id",
                                                      "Values": [vpc_id]}])['NetworkAcls']
            # [{'Associations': [{'NetworkAclAssociationId': 'aclassoc-02258f01281a3a9bf', 'NetworkAclId': 'acl-0384fe549b54118f5', 'SubnetId': 'subnet-00b6d2337ca24f7fc'}, {'NetworkAclAssociationId': 'aclassoc-0a33a6474b194cfeb', 'NetworkAclId': 'acl-0384fe549b54118f5', 'SubnetId': 'subnet-09f29ca8eb6da975d'}, {'NetworkAclAssociationId': 'aclassoc-04d8c76a6ed5c11d7', 'NetworkAclId': 'acl-0384fe549b54118f5', 'SubnetId': 'subnet-01880e304d2c7795b'}], 'Entries': [{'CidrBlock': '0.0.0.0/0', 'Egress': True, 'Protocol': '-1', 'RuleAction': 'allow', 'RuleNumber': 100}, {'CidrBlock': '0.0.0.0/0', 'Egress': True, 'Protocol': '-1', 'RuleAction': 'deny', 'RuleNumber': 32767}, {'CidrBlock': '0.0.0.0/0', 'Egress': False, 'Protocol': '-1', 'RuleAction': 'allow', 'RuleNumber': 100}, {'CidrBlock': '0.0.0.0/0', 'Egress': False, 'Protocol': '-1', 'RuleAction': 'deny', 'RuleNumber': 32767}], 'IsDefault': True, 'NetworkAclId': 'acl-0384fe549b54118f5', 'Tags': [], 'VpcId': 'vpc-0a854586a432f8f99', 'OwnerId': '309438011664'}]
            acl_type = properties.get('acl_type', '')
            for acl in acl_list:
                acl_id = acl['NetworkAclId']
                if acl_type in acl_id:
                    check_result_acl = check_expected_properties(expected_properties, acl)
                    print(f'Name: {acl_id} requirement check: {check_result_acl}')
                else:
                    print(f'Name: {acl_id} has no check!')

        print_summary
        print_resource_summary(resource_name, acl_list)


    def check_internet_gateway(self, rule):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))
        vpc_client = self.session.client("ec2", region_name=region)
        ec2 = self.session.resource('ec2', region_name=region)
        vpc_list = list(ec2.vpcs.filter(Filters=[]))
        for vpc in vpc_list:
            vpc_id = vpc.id
            igw_list = vpc_client.describe_internet_gateways(
                                Filters=[{"Name": "attachment.vpc-id",
                                "Values": [vpc_id]}])['InternetGateways']
            # [{'Attachments': [{'State': 'available', 'VpcId': 'vpc-0a854586a432f8f99'}], 'InternetGatewayId': 'igw-098d3e75355126e59', 'OwnerId': '309438011664', 'Tags': []}]
            igw_type = properties.get('igw_type', '')
            for igw in igw_list:
                igw_id = igw['InternetGatewayId']
                if igw_type in igw_id:
                    check_result_igw = check_expected_properties(expected_properties, igw)
                    print(f'Name: {igw_id} requirement check: {check_result_igw}')
                else:
                    print(f'Name: {igw_id} has no check!')

        print_summary
        print_resource_summary(resource_name, igw_list)

    def check_s3(self, rule):

        expected_properties, properties, resource_name, region = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name', 'region'))

        s3 = self.session.client('s3', region_name=region)
        bucket_list = s3.list_buckets()['Buckets']
        bucket_type = properties.get('bucket_type', '')
        for bucket in bucket_list:
            bucket_name = bucket['Name']

            if bucket_type in bucket_name:
                check_result_bucket = check_expected_properties(expected_properties, bucket)
                print(f'Name: {bucket_name} requirement check: {check_result_bucket}')
            else:
                print(f'Name: {bucket_name} has no check!')

        print_summary
        print_resource_summary(resource_name, bucket_list)



    def check_secrets_manager(self, rule):

        resource_name, region = get_properties(rule, elements = ('resource_name', 'region'))

        secrets_manager = boto3.client('secretsmanager', region_name=region)


        secrets = secrets_manager.list_secrets()['SecretList']

        print_summary
        print(f'The {resource_name} is a SaaS Service.')
        print_resource_summary(resource_name, secrets)


    def check_cloud_watch(self, rule):

        resource_name, region = get_properties(rule, elements = ('resource_name', 'region'))

        secrets_manager = boto3.client('secretsmanager', region_name=region)


        secrets = secrets_manager.list_secrets()['SecretList']

        print_summary
        print(f'The {resource_name} is a SaaS Service.')
        print_resource_summary(resource_name, secrets)


    def check_backup_services(self, rule):

        resource_name, region = get_properties(rule, elements = ('resource_name', 'region'))

        backup_services = boto3.client('backup', region_name=region)

        backup_plan_list = backup_services.list_backup_plans()['BackupPlansList']
        backup_selection_list = []

        print_summary
        print(f'The {resource_name} is a SaaS Service.')
        print_resource_summary("Number of Backup Plans", backup_plan_list)

        for bpl in backup_plan_list:
            backup_selection_list = backup_services.list_backup_selections(backup_plan_list=bpl['id'])['BackupPlansList']
            backup_selection_name = "Backup Selection" + backup_selection_list['name']
            print_resource_summary(backup_selection_name, backup_selection_list)



