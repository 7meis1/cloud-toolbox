from datetime import datetime
from config_admin.backend.git_backend import GitBackend
from config_admin.misc.enums import CloudProvider
from config_admin.misc.utils import get_cloud_provider

class ConfigService:

    def create_snapshot(self, cloud_provider: CloudProvider, subscription, resource_group_list):
        provider = get_cloud_provider(cloud_provider)
        provider.get_config(subscription, resource_group_filter=resource_group_list)

        now = datetime.now()
        dt_string = now.strftime("%d.%m.%Y %H:%M:%S")

        repo = GitBackend(provider.git_repo_path)
        repo.create_revision(f'Config snapshot: {dt_string}')

    def list_snapshots(self, cloud_provider: CloudProvider):
        provider = get_cloud_provider(cloud_provider)

        repo = GitBackend(provider.git_repo_path)
        repo.list_revisions()

    def summary_snapshot(self, cloud_provider: CloudProvider, commit):
        provider = get_cloud_provider(cloud_provider)

        repo = GitBackend(provider.git_repo_path)
        repo.list_statistics(commit)

    def list_snapshot(self, cloud_provider: CloudProvider, commit):
        provider = get_cloud_provider(cloud_provider)

        repo = GitBackend(provider.git_repo_path)
        print('Actual commit info:')
        repo.list_revision(commit)
        print('Actual commit statistics:')
        repo.list_statistics(commit)
        print('Actual commit revision:')
        diff = repo.diff_revision(commit, 'HEAD')
        for delta in diff.deltas:
            print(delta.new_file.path, delta.status_char())
