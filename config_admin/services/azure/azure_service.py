import os
from azure.identity import AzureCliCredential
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.resource.resources.models import ExportTemplateRequest
from azure.mgmt.network.v2022_01_01 import NetworkManagementClient
from azure.mgmt.storage.v2022_09_01 import StorageManagementClient
from azure.mgmt.keyvault.v2022_07_01 import KeyVaultManagementClient
from azure.mgmt.loganalytics import LogAnalyticsManagementClient
from azure.mgmt.recoveryservices import RecoveryServicesClient
from config_admin.misc.const import DEFAULT_CONFIG_PATH
from config_admin.misc.const import AZURE_CONFIG_PATH
from config_admin.misc.file_helper import write_config
from config_admin.misc.helper import *
from config_admin.services.abstract_cloud_service import AbstractCloudService

# According to:
# https://learn.microsoft.com/en-us/azure/developer/python/sdk/azure-sdk-overview

class AzureService(AbstractCloudService):

    def __init__(self) -> None:
        self.credential = AzureCliCredential()

    def get_config(self, subscription: dict, resource_group_filter: list):

        resource_mgmt_client = ResourceManagementClient(self.credential, subscription)

        resource_group_list = resource_mgmt_client.resource_groups.list()

        # if filter is empty export all resource groups
        if resource_group_filter is None:
            resource_group_filter = resource_group_list

        for item in resource_group_list:
            if item in resource_group_filter:
                poller  = resource_mgmt_client.resource_groups.begin_export_template(resource_group_name=item.name, parameters=ExportTemplateRequest(resources=['*']))
                result = poller.result()
                file_name = item.name + '.json'
                write_config(self.git_repo_filepath(file_name=file_name), result.template)

    def git_repo_filepath(self, file_name):
        return os.path.join(DEFAULT_CONFIG_PATH, AZURE_CONFIG_PATH, file_name)

    @property
    def git_repo_path(self):
        return os.path.join(DEFAULT_CONFIG_PATH, AZURE_CONFIG_PATH)

    @staticmethod
    def __get_subscription_id(rule, subscription):
        subscriptions_ref = rule.get('subscriptions_ref', '')
        return subscription[subscriptions_ref]

    def check_resource_group(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        resource_mgmt_client = ResourceManagementClient(self.credential, subscription_id)
        resource_groups = resource_mgmt_client.resource_groups.list()

        expected_properties, properties, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name'))
        rgroup_type = properties.get('rgroup_type', '')

        resource_group_list = list(resource_groups)

        for rg in resource_group_list:
            if rgroup_type in rg.name:
                check_result_rg = check_expected_properties(expected_properties, rg)
                print(f'Name: {rg.name} requirement check: {check_result_rg}')
            else:
                print(f'Name: {rg.name} has no check!')
        print_summary
        print_resource_summary(resource_name[0], resource_group_list)

    def check_vnet_snet(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))
        vnet_type = properties.get('vnet_type', '')
        snet_type = properties.get('snet_type', '')

        vnet_list = list(network_mgmt_client.virtual_networks.list(resource_group_name=resource_group))
        snet_list = list() # initialize list in case of no subnet is available
        for vnet in vnet_list:
            check_result_vnet = None
            if vnet_type in vnet.name:
                check_result_vnet = check_expected_properties(expected_properties, vnet)
                print(f'Name: {vnet.name} requirement check: {check_result_vnet}')
                snet_list = list(network_mgmt_client.subnets.list(resource_group_name=resource_group, virtual_network_name=vnet.name))
                for snet in snet_list:
                    check_result_snet = None
                    if snet_type in snet.name:
                        check_result_snet = check_expected_properties(expected_properties, snet)
                        print(f'Name: {snet.name} requirement check: {check_result_snet}')
                    else:
                        print(f'Name: {snet.name} has no check!')
            else:
                print(f'Name: {vnet.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], vnet_list)
        print_resource_summary(resource_name[1], snet_list)

    def check_nsg(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))

        nsg_type = properties.get('nsg_type', '')

        nsg_list = list(network_mgmt_client.network_security_groups.list(resource_group))

        for nsg in nsg_list:
            check_result = None
            if nsg_type in nsg.name:
                check_result = check_expected_properties(expected_properties, nsg)
                print(f'Name: {nsg.name} requirement check: {check_result}' )
            else:
                print(f'Name: {nsg.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], nsg_list)

    def check_route_table(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))

        route_table_type = properties.get('route_table_type', '')

        route_table_list = list(network_mgmt_client.route_tables.list(resource_group))
        for route_table in route_table_list:
            check_result = None
            if route_table_type in route_table.name:
                check_result = check_expected_properties(expected_properties, route_table)
                print(f'Name: {route_table.name} requirement check: {check_result}' )
            else:
                print(f'Name: {route_table.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], route_table_list)


    def check_nat_gateway(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))

        nat_gateway_type = properties.get('nat_gateway_type', '')

        nat_gateway_list = list(network_mgmt_client.nat_gateways.list(resource_group_name=resource_group))
        for nat_gateway in nat_gateway_list:
            check_result = None
            if nat_gateway_type in nat_gateway.name:
                check_result = check_expected_properties(expected_properties, nat_gateway)
                print(f'Name: {nat_gateway.name} requirement check: {check_result}' )
            else:
                print(f'Name: {nat_gateway.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], nat_gateway_list)

    def check_public_ip(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))

        public_ip_type = properties.get('public_ip_type', '')

        public_ip_list = list(network_mgmt_client.public_ip_addresses.list(resource_group_name=resource_group))
        for public_ip in public_ip_list:
            check_result = None
            if public_ip_type in public_ip.name:
                check_result = check_expected_properties(expected_properties, public_ip)
                print(f'Name: {public_ip.name} requirement check: {check_result}' )
            else:
                print(f'Name: {public_ip.name} has no check!' )

        print_summary
        print_resource_summary(resource_name[0], public_ip_list)

    def check_public_ip_prefix(self, rule: dict, subscription: dict):
        subscription_id = self.__get_subscription_id(rule, subscription)

        network_mgmt_client = NetworkManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_group, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_group', 'resource_name'))

        public_ip_prefix_type = properties.get('public_ip_prefix_type', '')

        public_ip_prefix_list = list(network_mgmt_client.public_ip_prefixes.list(resource_group_name=resource_group))
        for public_ip_prefix in public_ip_prefix_list:
            check_result = None
            if public_ip_prefix_type in public_ip_prefix.name:
                check_result = check_expected_properties(expected_properties, public_ip_prefix)
                print(f'Name: {public_ip_prefix.name} requirement check: {check_result}' )
            else:
                print(f'Name: {public_ip_prefix.name} has no check!' )

        print_summary
        print_resource_summary(resource_name[0], public_ip_prefix_list)

    def check_storage(self, rule: dict, subscription: dict):

        subscription_id = self.__get_subscription_id(rule, subscription)
        storage_mgmt_client = StorageManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name'))
        storage_type = properties.get('storage_type', '')

        storage_account_list = list(storage_mgmt_client.storage_accounts.list())

        for storage in storage_account_list:
            check_result = None
            if storage_type in storage.name:
                check_result = check_expected_properties(expected_properties, storage)
                print(f'Name: {storage.name} requirement check: {check_result}' )
            else:
                print(f'Name: {storage.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], storage_account_list)

    def check_key_vault(self, rule: dict, subscription: dict):

        subscription_id = self.__get_subscription_id(rule, subscription)
        key_vault_mgmt_client = KeyVaultManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name'))
        key_vault_type = properties.get('vault_type', '')

        key_vault_list = list(key_vault_mgmt_client.vaults.list())
        for key_vault in key_vault_list:
            check_result = None
            if key_vault_type in key_vault.name:
                check_result = check_expected_properties(expected_properties, key_vault)
                print(f'Name: {key_vault.name} requirement check: {check_result}' )
            else:
                print(f'Name: {key_vault.name} has no check!')

        print_summary
        print_resource_summary(resource_name[0], key_vault_list)

    def check_log_analytics(self, rule: dict, subscription: dict):

        subscription_id = self.__get_subscription_id(rule, subscription)
        log_analytics_mgmt_client = LogAnalyticsManagementClient(self.credential, subscription_id)

        expected_properties, properties, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name'))
        law_type = properties.get('vault_type', '')

        log_analytics_list = list(log_analytics_mgmt_client.workspaces.list())

        for law in log_analytics_list:
            check_result = None
            if law_type in law.name:
                check_result = check_expected_properties(expected_properties, law)
                print(f'Name: {law.name} requirement check: {check_result}' )
            else:
                print(f'Name: {law.name} has no check!' )

        print_summary
        print_resource_summary(resource_name[0], log_analytics_list)

    def check_backup_vault(self, rule: dict, subscription: dict):

        subscription_id = self.__get_subscription_id(rule, subscription)
        backup_mgmt_client = RecoveryServicesClient(self.credential, subscription_id)

        expected_properties, properties, resource_name = get_properties(rule, elements = ('expected_properties', 'parameters', 'resource_name'))
        vault_type = properties.get('vault_type', '')

        backup_vault_list = list(backup_mgmt_client.vaults.list_by_subscription_id())

        for vault in backup_vault_list:
            check_result = None
            if vault_type in vault.name:
                check_result = check_expected_properties(expected_properties, vault)
                print(f'Name: {vault.name} requirement check: {check_result}' )
            else:
                print(f'Name: {vault.name} has no check!' )

        print_summary
        print_resource_summary(resource_name[0], backup_vault_list)

