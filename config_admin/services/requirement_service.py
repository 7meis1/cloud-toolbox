from rich import print

from config_admin.misc.enums import CloudProvider
from config_admin.misc.file_helper import get_json_data
from config_admin.misc.utils import get_cloud_provider
from config_admin.services.azure.azure_service import AzureService
from config_admin.services.aws.aws_service import AWSService

class RequirementService:

    def __init__(self) -> None:
        pass

    def check_requirement(self, config_file_path: str):
        config = get_json_data(config_file_path)
        subscriptions = config.get('subscriptions', {})
        rules = config.get('rules', [])

        cloud_provider = CloudProvider(config.get('global', {}).get('cloud_provider'))

        if cloud_provider == CloudProvider.azure:
            cloud: AzureService = get_cloud_provider(cloud_provider)

            for rule in rules:
                self.check_rule_azure(cloud, rule, subscriptions)

        elif cloud_provider == CloudProvider.aws:
            cloud: AWSService = get_cloud_provider(cloud_provider)
            for rule in rules:
                self.check_rule_aws(cloud, rule)


    def check_rule_azure(self, cloud, rule: dict, subscriptions: dict):
        check_name = rule.get('check_name')

        print(f'[blue]>>> running check: {check_name}[/blue]')
        match check_name:
            case 'check_resource_group':
                return cloud.check_resource_group(rule, subscriptions)
            case 'check_vnet_snet':
                return cloud.check_vnet_snet(rule, subscriptions)
            case 'check_nsg':
                return cloud.check_nsg(rule, subscriptions)
            case 'check_route_table':
                return cloud.check_route_table(rule, subscriptions)
            case 'check_nat_gateway':
                return cloud.check_nat_gateway(rule, subscriptions)
            case 'check_public_ip':
                return cloud.check_public_ip(rule, subscriptions)
            case 'check_public_ip_prefix':
                return cloud.check_public_ip_prefix(rule, subscriptions)
            case 'check_storage':
                return cloud.check_storage(rule, subscriptions)
            case 'check_log_analytics':
                return cloud.check_log_analytics(rule, subscriptions)
            case 'check_key_vault':
                return cloud.check_key_vault(rule, subscriptions)
            case 'check_backup_vault':
                return cloud.check_backup_vault(rule, subscriptions)
            case _:
                print(f'no selector found for {check_name}')


    def check_rule_aws(self, cloud, rule: dict):
        check_name = rule.get('check_name')

        print(f'[blue]>>> running check: {check_name}[/blue]')
        match check_name:
            case 'check_vpc':
                return cloud.check_vpc(rule)
            case 'check_subnet':
                return cloud.check_subnet(rule)
            case 'check_rtb':
                return cloud.check_route_table(rule)
            case 'check_internet_gateway':
                return cloud.check_internet_gateway(rule)
            case 'check_security_group':
                return cloud.check_security_group(rule)
            case 'check_network_acl':
                return cloud.check_network_acl(rule)
            case 'check_secrets_manager':
                return cloud.check_secrets_manager(rule)
            case 'check_backup_services':
                return cloud.check_backup_services(rule)
            case 'check_cloud_watch':
                return cloud.check_cloud_watch(rule)
            case 'check_s3':
                return cloud.check_s3(rule)
            case _:
                print(f'no selector found for {check_name}')