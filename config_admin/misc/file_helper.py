import json
import os

from config_admin.misc.const import (
    DEFAULT_DATASTORE_PATH,
    DEFAULT_REQUIREMENTS_DATASTORE,
)

def get_json_data(path: str | None = None) -> dict:
    '''
    Get data to json file
    '''
    if path is None:
        path = os.path.join(DEFAULT_DATASTORE_PATH, DEFAULT_REQUIREMENTS_DATASTORE)

    with open(path, 'r') as file:
        return json.load(file)


def write_config(file: str, content: dict) -> dict:
    '''
    Write data to config file
    '''
    print(file)
    json.dump(content, open(file, 'w'), indent=4)
