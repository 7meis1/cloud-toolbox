from enum import unique, Enum, auto

class __AutoNameLowercase(Enum):
    def _generate_next_value_(name, start, count, last_values) -> str:
        return name.lower()


@unique
class CloudProvider(__AutoNameLowercase):
    aws: str = auto()
    azure: str = auto()
