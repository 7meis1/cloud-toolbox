from __future__ import annotations
from dataclasses import dataclass


@dataclass
class ResourceCheckResult:
    summary: str
    success_count: int
    fail_count: int
    property_count: int

    @staticmethod
    def from_check_result(result_summary: str, expected_properties: dict, success_count: int) -> ResourceCheckResult:
        property_count = len(expected_properties)
        return ResourceCheckResult(
            summary=result_summary,
            success_count=success_count,
            fail_count=property_count-success_count,
            property_count=property_count,
        )

    def __repr__(self):
        return f'[{self.summary}, success={self.success_count} failed={self.fail_count}]'
