from config_admin.misc.enums import CloudProvider
from config_admin.services.aws.aws_service import AWSService
from config_admin.services.azure.azure_service import AzureService

def get_cloud_provider(cloud_selector):
    match cloud_selector:
        case CloudProvider.azure:
            return AzureService()
        case CloudProvider.aws:
            return AWSService()