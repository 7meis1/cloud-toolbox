##
# Default Config Values
##

# Path and config file location
DEFAULT_DATASTORE_PATH = "data"
DEFAULT_REQUIREMENTS_DATASTORE = "requirements.json"
DEFAULT_RULES_DATASTORE = "rules.json"
DEFAULT_CONFIG_PATH = "../"

# Git Repo Paths for cloud arch. config
AZURE_CONFIG_PATH = 'azure_config'
AWS_CONFIG_PATH = 'azure_config'

# Git parameters
DEFAULT_GIT_AUTHOR = "Cloud-Toolbox"
DEFAULT_GIT_EMAIL = "cloud-toolbox@example.com"