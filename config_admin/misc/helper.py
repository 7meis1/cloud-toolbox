from typing import Iterable
from config_admin.misc.dto import ResourceCheckResult

def check_expected_properties(expected_properties: dict, cloud_resource) -> ResourceCheckResult:
    result: list[str] = []
    success_count: int = 0
    for key, expected_value in expected_properties.items():
        try:
            if isinstance(cloud_resource, list):
                effective_value = str(cloud_resource[0].get(key))
            elif isinstance(cloud_resource, dict):
                effective_value = str(cloud_resource.get(key))
            else:
                effective_value = getattr(cloud_resource, key)

            if effective_value == expected_value:
                success_count += 1
                result.append(f'{key}=OK')
            else:
                result.append(f'{key}=FAILED (got: {effective_value})')
        except:
            print(f'Unknown property {key} for given resource {cloud_resource}')
    return ResourceCheckResult.from_check_result(' '.join(result), expected_properties, success_count)


def get_properties(rule, elements: Iterable):
    return (rule.get(element, {}) for element in elements)

def print_summary():
    print(f'Summary:')

def print_resource_summary(check_name: str, resource_list: list):
    print(f'    Total number of {check_name}: {len(resource_list)}')