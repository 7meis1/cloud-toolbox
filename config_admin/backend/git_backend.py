import pygit2
from pygit2 import GIT_SORT_TOPOLOGICAL
from config_admin.misc.const import DEFAULT_GIT_AUTHOR, DEFAULT_GIT_EMAIL

class GitBackend:

    def __init__(self, path) -> None:
        repo_path = pygit2.discover_repository(path)
        if repo_path is None:
            self.repo = pygit2.init_repository(repo_path)
        else:
            self.repo = pygit2.Repository(repo_path)

    def create_revision(self, message):
        '''
        Create a new revision
        According to: https://www.pygit2.org/index_file.html
        '''
        index = self.repo.index
        index.add_all()
        index.write()
        ref = "HEAD"
        author = pygit2.Signature(DEFAULT_GIT_AUTHOR, DEFAULT_GIT_EMAIL)
        committer = pygit2.Signature(DEFAULT_GIT_AUTHOR, DEFAULT_GIT_EMAIL)
        tree = index.write_tree()
        parents = []
        self.repo.create_commit(ref, author, committer, message, tree, parents)

    def diff_revision(self, revision_a: str, revision_b: str = 'HEAD'):
        return self.repo.diff(revision_a, revision_b)

    def list_revision(self, commit):
        '''
        Get a specific commit
        '''
        commit = self.repo.revparse_single(commit)
        print(f'CommitID: {commit.id} Commit Message: {commit.message}')

    def list_revisions(self):
        '''
        Get all commits
        '''
        for commit in self.repo.walk(self.repo.head.target, GIT_SORT_TOPOLOGICAL):
            print(f'CommitID: {commit.id} Commit Message: {commit.message}')

    def list_statistics(self, commit):
        '''
        Get statistics of a commit
        '''
        commit = self.repo.revparse_single(commit)
        # when commit is not head
        if self.repo.head != commit:
            diff = self.repo.diff(commit)
        else:
            diff = self.repo.diff(commit.parents[0], commit)

        print("Statistics of:")
        print(f'CommitID: {commit.id} Commit Message: {commit.message}')
        print(f'Insertions: {diff.stats.insertions} Deletions: {diff.stats.deletions} Files changed: {diff.stats.files_changed}')
        #print(f'Diff: {diff.line_stats}')
        # List all files in stat
        for e in commit.tree:
            print(e.name)
