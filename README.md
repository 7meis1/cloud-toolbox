# Cloud-Toolbox

This repository contains a 'Virtual Environment (venv)'. To load all modules please install and enable the 'venv' first.

```shell
python3 -m venv venv
```

**Activate venv on Windows**

```shell
source .venv/Scripts/activate
```

**Activate venv on Linux / macOS**

```shell
source .venv/bin/activate
```

**Installation**

Make sure all python modules are installed.

```python
python3 -m pip install -r requirements.txt
```

**Cloud-Tool usage**

```shell

#############
# Snapshots #
#############

# Create a snapshot from an Azure Architecture
python3 main.py snapshot create --cloud azure --subscription 00000000-0000-0000-0000-000000000000

# List all snapshots
python3 main.py snapshot list-all --cloud azure

# Get Summary of snapshot
python3 main.py snapshot summary --commit 3f5cf147f21d13d37c62eef378fe1286be3a45da --cloud azure

# Get single snapshot
python3 main.py snapshot list --commit 3f5cf147f21d13d37c62eef378fe1286be3a45da --cloud azure

################
# Requirements #
################

# Check Azure requirements
python3 main.py requirement check --config data/requirements_azure.json

# Check Azure requirements on specific subscription
python3 main.py requirement check --config data/requirements_azure.json --subscription 00000000-0000-0000-0000-000000000000

# Check AWS requirements
typer main.py requirement check --config data/requirements_aws.json

```
